# Bubble Bags for Hash Making
What's bubble hash and what are bubble bags?
Bubble hash can be explained as hashish that bubbles when it's smoked. Smoking cigarettes bubble hash though a bong, vaporizer, in a joint or a blunt all lead to a bubbling impact in the combusting hashish. This visual and audible bubbling effect is what deemed this kind of hash the nickname ‘bubble hash’.
Bubble Hash 's been around for hundreds of years, dating back again to certain places like Morocco and India historically. In the 80’s and 70’s hash smuggling became extremely popular in Europe, and some varieties of bubble hash got legendary.
Unlike many other cannabis concentrates, bubble hash is not made out of any solvents. This makes it a safe and clean form of hash, in comparison to such extraction methods as BHO (butane hash essential oil). Making bubble hash is performed with ice, screen bags, marijuana and water. That’s it, basic.
Some bubble hash is manufactured in a different way, such as for example collecting the resin on human skin or additional scrapeable surface. Nevertheless, in this report we’re just going to focus on the popular making hash with ice technique.
What are bubble bags?
To make bubble hash as described above, you shall need screens to filter the varying grades of hash consistency you yield. How do you do that? Simple, buy bubble luggage.
Bubble bags are screened bags that can let water seep while catching the valuable bubble hash inside though. Bubble bags come in a variety of micron screen sizes, as well as qualities and quantity of luggage used.
Bubble bags can individually end up being bought, or in a package. The best Bubble bags for hash https://moldresistantstrains.com/6-best-bubble-bag-kits-review-how-to-make-hash-with-ice/ kits range from 2 bags all the way up to 10 bag kits. I don’t understand anyone who use 10 screens in a bucket while making bubble hash, but probably they’re out there, in case you are comment below 😊.
Do I take advantage of leaf or marijuana to make bubble hash?
Many cannabis cultivators help to make bubble hash from their spare leaf trimmings. This is fine perfectly, and yields a stony bubble hash with a lovely flavor.
A far more potent method to make bubble hash is by using straight buds. Take note though, it might take additional time and consecutive stirrings to extract the cannabinoids off of the buds. All too often people try to produce a quick operate of bubble hash with buds, and their first yields come out disappointing. Place some elbow grease involved with it, and you may even consider grinding the buds beforehand as THC and additional cannabinoids can become trapped within a big bud.
How do you use bubble hand bags?
In order to use bubble bags, you should first have a bucket to stir the marijuana up as well as ice and water. You are able to do this in a 5-gallon bucket, stirring with a handle of some kind as you churn the blend together. Most people churn the marijuana ice cube mix for at least 10 minutes before doing your first operate for yields.
Contrary to public opinion, you don’t have to churn the mix within the bubble bags. Churning and using drive In the bubble hand bags is a surefire method to increase the deterioration of the hand bags. Rather, churn into an unlined bucket that may handle the brute force of a stick slamming against it repeatedly.
How do you produce bubble hash?
After you have mixed your marijuana leaf or buds together with your ice water and cube mixture, making bubble hash is really as simple as loading the bags up with the mix. You can line the 5-gallon, 3-gallon, ect. sized bucket together with your likewise sized bubble hand bags. Loosest screens continue best and finest displays continue the bottom.
Pull up each screen layer individually and let the water drain from it. The first display layer you’ll pull up is the wide screen meant for separating the ice, water and cannabis from the hashish. Don’t toss this apart. You can do it again the stirring process for a number of times afterwards until it’s completely spent.
As you get to the lower screens with hash in them, make sure the water drains completely and straight down although screen. When the last quantity of dampness is trying to truly get you, you might like to press and squeeze it out good. 
A towel could be set by you down on a table, contain the screened part of the bubble bag in your hand and sandwich it among layers of the towel. Pressure to lessen moisture content apply.
Open up the bubble bag and retrieve you hash yield with a spoon or related scraping gadget. Congrats, you merely produced bubble hash!




